package br.com.bobycachoeira.services;

import br.com.bobycachoeira.controller.Pergunta2Controller;
import br.com.bobycachoeira.model.Pergunta2DTO;

import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;

/**
 * Created by boby on 18/10/15.
 */

@Path("/pergunta2")
@Consumes("application/json")
@Produces("application/json")
public class Pergunta2Service {

    @Inject
    private Pergunta2Controller controller;

    @SuppressWarnings("PathAnnotation")
    @GET
    @Path("")
    public Pergunta2DTO retornaDados() {

        return controller.retornaDados();
    }
}
