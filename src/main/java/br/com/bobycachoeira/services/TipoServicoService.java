package br.com.bobycachoeira.services;

import br.com.bobycachoeira.controller.TipoServicoController;
import br.com.bobycachoeira.model.TipoServico;

import javax.inject.Inject;
import javax.ws.rs.*;
import java.util.List;

/**
 * Created by boby on 25/09/15.
 */
@Path("/tiposervico")
@Consumes("application/json")
@Produces("application/json")
public class TipoServicoService {

    @Inject
    private TipoServicoController controller;

    @POST
    @Path("/adicionar")
    public TipoServico adicionar(TipoServico servico) {
        return controller.save(servico);
    }

    @PUT
    @Path("/alterar")
    public TipoServico alterar(final TipoServico servico) {
        return controller.update(servico);
    }

	/*@GET
	@Path("/novo")
	public void form() {
	}*/

    @SuppressWarnings("PathAnnotation")
    @GET
    @Path("")
    public List<TipoServico> listar() {

        return controller.listAll();
    }

    @GET
    @Path("/pesquisar/{id}")
    public TipoServico listar(@PathParam("id") Long id) {
        return controller.getById(id);
    }

    @DELETE
    @Path("/excluir/{id}")
    public void remover(@PathParam("id") Long id) {
        controller.delete(id);
    }

    @GET
    @Path("/visualizar")
    public TipoServico visualizar(final TipoServico servico) {
        return controller.getById(servico.getId());
    }
}
