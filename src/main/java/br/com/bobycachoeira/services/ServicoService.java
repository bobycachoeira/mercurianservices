package br.com.bobycachoeira.services;

import br.com.bobycachoeira.controller.ServicoController;
import br.com.bobycachoeira.model.Servico;

import javax.inject.Inject;
import javax.ws.rs.*;
import java.util.List;

/**
 * Created by boby on 25/09/15.
 */
@Path("/servico")
@Consumes("application/json")
@Produces("application/json")
public class ServicoService {

    @Inject
    private ServicoController controller;

    @POST
    @Path("/adicionar")
    public Servico adicionar(Servico servico) throws Exception {
        return controller.save(servico);
    }

    @PUT
    @Path("/alterar")
    public Servico alterar(final Servico servico) {
        return controller.update(servico);
    }

	/*@GET
	@Path("/novo")
	public void form() {
	}*/

    @SuppressWarnings("PathAnnotation")
    @GET
    @Path("")
    public List<Servico> listar() {

        return controller.listAll();
    }

    @GET
    @Path("/pesquisar/{id}")
    public Servico listar(@PathParam("id") Long id) {
        return controller.getById(id);
    }

    @DELETE
    @Path("/excluir/{id}")
    public void remover(@PathParam("id") Long id) {
        controller.delete(id);
    }

    @GET
    @Path("/visualizar")
    public Servico visualizar(final Servico servico) {
        return controller.getById(servico.getId());
    }

}