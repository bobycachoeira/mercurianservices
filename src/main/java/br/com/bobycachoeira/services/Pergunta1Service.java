package br.com.bobycachoeira.services;

import br.com.bobycachoeira.controller.Pergunta1Controller;
import br.com.bobycachoeira.model.Pergunta1DTO;

import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;

/**
 * Created by boby on 18/10/15.
 */

@Path("/pergunta1")
@Consumes("application/json")
@Produces("application/json")
public class Pergunta1Service {

    @Inject
    private Pergunta1Controller controller;

    @SuppressWarnings("PathAnnotation")
    @GET
    @Path("")
    public Pergunta1DTO retornaDados() {

        return controller.retornaDados();
    }
}
