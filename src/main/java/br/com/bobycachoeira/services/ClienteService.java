package br.com.bobycachoeira.services;

import javax.ws.rs.*;

import javax.inject.Inject;

import br.com.bobycachoeira.controller.ClienteController;
import br.com.bobycachoeira.model.Cliente;

import java.util.List;


@Path("/cliente")
@Consumes("application/json")
@Produces("application/json")
public class ClienteService {

    @Inject
    private ClienteController controller;
    /*private Long id;*/

    @POST
    @Path("/adicionar")
    public Cliente adicionar(Cliente cliente) {
        return controller.save(cliente);


    }

    @PUT
    @Path("/alterar")
    public Cliente alterar(final Cliente cliente) {
        return controller.update(cliente);
    }

	/*@GET
	@Path("/novo")
	public void form() {
	}*/

    @SuppressWarnings("PathAnnotation")
    @GET
    @Path("")
    public List<Cliente> listar() {
        return controller.listAll();
    }

    @GET
    @Path("/pesquisar/{id}")
    public Cliente listar(@PathParam("id") Long id) {
        return controller.getById(id);
    }

    @DELETE
    @Path("/excluir/{id}")
    public void remover(@PathParam("id") Long id) {
        controller.delete(id);
    }

    @GET
    @Path("/visualizar")
    public Cliente visualizar(final Cliente cliente) {
        return controller.getById(cliente.getId());
    }
}
