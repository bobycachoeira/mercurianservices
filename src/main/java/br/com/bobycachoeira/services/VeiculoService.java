package br.com.bobycachoeira.services;

import br.com.bobycachoeira.controller.VeiculoController;
import br.com.bobycachoeira.model.Veiculo;

import javax.inject.Inject;
import javax.ws.rs.*;
import java.util.List;

/**
 * Created by boby on 25/09/15.
 */
@Path("/veiculo")
@Consumes("application/json")
@Produces("application/json")
public class VeiculoService {

    @Inject
    private VeiculoController controller;

    @POST
    @Path("/adicionar")
    public Veiculo adicionar(Veiculo veiculo) {
        return controller.save(veiculo);
    }

    @PUT
    @Path("/alterar")
    public Veiculo alterar(final Veiculo veiculo) {

        return controller.update(veiculo);
    }

	/*@GET
	@Path("/novo")
	public void form() {
	}*/

    @SuppressWarnings("PathAnnotation")
    @GET
    @Path("")
    public List<Veiculo> listar() {

        return controller.listAll();
    }

    @SuppressWarnings("PathAnnotation")
    @GET
    @Path("/pesquisarporcliente/{id}")
    public List<Veiculo> listarPorCliente(@PathParam("id") Long id) {

        return controller.listaPorCliente(id);
    }

    @GET
    @Path("/pesquisar/{id}")
    public Veiculo listar(@PathParam("id") Long id) {
        return controller.getById(id);
    }

    @DELETE
    @Path("/excluir/{id}")
    public void remover(@PathParam("id") Long id) {
        controller.delete(id);
    }

    @GET
    @Path("/visualizar")
    public Veiculo visualizar(final Veiculo veiculo) {
        return controller.getById(veiculo.getId());
    }
}
