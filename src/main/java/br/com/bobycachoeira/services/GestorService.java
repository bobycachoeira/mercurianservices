package br.com.bobycachoeira.services;

import br.com.bobycachoeira.controller.GestorController;
import br.com.bobycachoeira.model.Gestor;

import javax.inject.Inject;
import javax.ws.rs.*;
import java.util.List;

/**
 * Created by boby on 12/10/15.
 */

@Path("/gestor")
@Consumes("application/json")
@Produces("application/json")
public class GestorService {

    @Inject
    private GestorController controller;
    /*private Long id;*/

    @POST
    @Path("/adicionar")
    public Gestor adicionar(Gestor gestor) {
        return controller.save(gestor);


    }

    @PUT
    @Path("/alterar")
    public Gestor alterar(final Gestor gestor) {
        return controller.update(gestor);
    }

	/*@GET
	@Path("/novo")
	public void form() {
	}*/

    @SuppressWarnings("PathAnnotation")
    @GET
    @Path("")
    public List<Gestor> listar() {
        return controller.listAll();
    }

    @GET
    @Path("/pesquisar/{id}")
    public Gestor listar(@PathParam("id") Long id) {
        return controller.getById(id);
    }

    @DELETE
    @Path("/excluir/{id}")
    public void remover(@PathParam("id") Long id) {
        controller.delete(id);
    }

    @GET
    @Path("/visualizar")
    public Gestor visualizar(final Gestor gestor) {
        return controller.getById(gestor.getId());
    }
}
