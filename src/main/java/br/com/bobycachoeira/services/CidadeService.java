package br.com.bobycachoeira.services;

import br.com.bobycachoeira.controller.CidadeController;
import br.com.bobycachoeira.model.Cidade;

import javax.inject.Inject;
import javax.ws.rs.*;
import java.util.List;

/**
 * Created by boby on 22/09/15.
 */
@Path("/cidade")
@Consumes("application/json")
@Produces("application/json")
public class CidadeService {
    @Inject
    private CidadeController controller;

    @POST
    @Path("/adicionar")
    public Cidade adicionar(Cidade cidade) {
       /* return Response.ok()//200
                .entity(controller.save(cidade))
                .header("Access-Control-Allow-Origin", "*")
                .header("Access-Control-Allow-Methods", "GET, POST, DELETE, PUT")
                .allow("OPTIONS").build();*/
        return controller.save(cidade);
    }

    @PUT
    @Path("/alterar")
    public Cidade alterar(final Cidade cidade) {
        return controller.update(cidade);
    }


    @SuppressWarnings("PathAnnotation")
    @GET
    @Path("")
    public List<Cidade> listar() {

        return controller.listAll();
    }

    @GET
    @Path("/pesquisar/{id}")
    public Cidade listar(@PathParam("id") Long id) {
        return controller.getById(id);
    }

    @DELETE
    @Path("/excluir/{id}")
    public void remover(@PathParam("id") Long id) {
        controller.delete(id);
    }

    @GET
    @Path("/visualizar")
    public Cidade visualizar(final Cidade cidade) {
        return controller.getById(cidade.getId());
    }
}
