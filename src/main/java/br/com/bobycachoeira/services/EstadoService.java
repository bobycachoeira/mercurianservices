package br.com.bobycachoeira.services;

import br.com.bobycachoeira.controller.EstadoController;
import br.com.bobycachoeira.model.Estado;

import javax.inject.Inject;
import javax.ws.rs.*;
import java.util.List;

/**
 * Created by boby on 25/09/15.
 */
@Path("/estado")
@Consumes("application/json")
@Produces("application/json")
public class EstadoService {

    @Inject
    private EstadoController controller;

    @POST
    @Path("/adicionar")
    public Estado adicionar(Estado estado) {
        return controller.save(estado);
    }

    @PUT
    @Path("/alterar")
    public Estado alterar(final Estado estado) {

        return controller.update(estado);
    }

	/*@GET
	@Path("/novo")
	public void form() {
	}*/

    @SuppressWarnings("PathAnnotation")
    @GET
    @Path("")
    public List<Estado> listar() {

        return controller.listAll();
    }

    @GET
    @Path("/pesquisar/{id}")
    public Estado listar(@PathParam("id") Long id) {
        return controller.getById(id);
    }

    @DELETE
    @Path("/excluir/{id}")
    public void remover(@PathParam("id") Long id) {
        controller.delete(id);
    }

    @GET
    @Path("/visualizar")
    public Estado visualizar(final Estado estado) {
        return controller.getById(estado.getId());
    }
}
