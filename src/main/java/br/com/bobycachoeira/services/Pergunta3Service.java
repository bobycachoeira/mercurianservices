package br.com.bobycachoeira.services;

import br.com.bobycachoeira.controller.Pergunta3Controller;
import br.com.bobycachoeira.model.Pergunta3DTO;

import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;

/**
 * Created by boby on 18/10/15.
 */

@Path("/pergunta3")
@Consumes("application/json")
@Produces("application/json")
public class Pergunta3Service {
    @Inject
    private Pergunta3Controller controller;

    @SuppressWarnings("PathAnnotation")
    @GET
    @Path("")
    public Pergunta3DTO retornaDados() {

        return controller.retornaDados();
    }

}
