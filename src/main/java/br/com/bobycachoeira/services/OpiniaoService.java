package br.com.bobycachoeira.services;

import br.com.bobycachoeira.controller.OpiniaoController;
import br.com.bobycachoeira.model.Opiniao;

import javax.inject.Inject;
import javax.ws.rs.*;
import java.util.List;

/**
 * Created by boby on 12/10/15.
 */

@Path("/opiniao")
@Consumes("application/json")
@Produces("application/json")
public class OpiniaoService {
    @Inject
    private OpiniaoController controller;
    /*private Long id;*/

    @POST
    @Path("/adicionar")
    public Opiniao adicionar(Opiniao opiniao) {
        return controller.save(opiniao);


    }

    @PUT
    @Path("/alterar")
    public Opiniao alterar(final Opiniao opiniao) {
        return controller.update(opiniao);
    }

    @PUT
    @Path("/clienteAlterar")
    public Opiniao clienteAlterar(final Opiniao opiniao) {
        return controller.clienteAlterar(opiniao);
    }

    @PUT
    @Path("/baixar")
    public Opiniao baixar(final Opiniao opiniao) {
        return controller.baixa(opiniao);
    }


    @SuppressWarnings("PathAnnotation")
    @GET
    @Path("")
    public List<Opiniao> listar() {
        return controller.listAll();
    }


    @GET
    @Path("/pesquisar/{id}")
    public Opiniao listar(@PathParam("id") String id) {
        return controller.getById(id);
    }


    @DELETE
    @Path("/excluir/{id}")
    public void remover(@PathParam("id") Long id) {
        controller.delete(id);
    }

    /*@GET
    @Path("/visualizar")
    public Opiniao visualizar(final Opiniao opiniao) {
        return controller.getById(opiniao.getId());
    }*/
}
