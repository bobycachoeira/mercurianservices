package br.com.bobycachoeira.dao;

import br.com.bobycachoeira.model.Gestor;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import java.util.List;

/**
 * Created by boby on 12/10/15.
 */
@Stateless
public class GestorDAO {

    @PersistenceContext
    private EntityManager em;

    public Gestor save(Gestor gestor) {
        em.persist(gestor);
        return gestor;
    }

    public void update(Gestor gestor) {
        em.merge(gestor);
    }

    public void delete(Long codigo) {
        em.remove(em.getReference(Gestor.class, codigo));
    }

    public List<Gestor> listAll() {
        return em.createQuery("select o from Gestor o", Gestor.class).getResultList();
    }

    public Gestor getById(Long id) {
        TypedQuery<Gestor> query = em.createQuery("select o from Gestor o where o.id=:id", Gestor.class);
        query.setParameter("id", id);
        return query.getSingleResult();
    }

    public List<Gestor> getByName(String name) {
        TypedQuery<Gestor> query = em.createQuery("select o from Gestor o where o.nome like :name", Gestor.class);
        query.setParameter("name", "%"+name+"%");
        return query.getResultList();
    }
}
