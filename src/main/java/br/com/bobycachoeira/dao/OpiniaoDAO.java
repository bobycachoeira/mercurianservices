package br.com.bobycachoeira.dao;

import br.com.bobycachoeira.model.Opiniao;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import java.util.List;

/**
 * Created by boby on 12/10/15.
 */

@Stateless
public class OpiniaoDAO {

    @PersistenceContext
    private EntityManager em;

    public Opiniao save(Opiniao opiniao) {
        em.persist(opiniao);
        return opiniao;
    }

    public void update(Opiniao opiniao) {
        em.merge(opiniao);
    }

    public void delete(Long codigo) {
        em.remove(em.getReference(Opiniao.class, codigo));
    }

    public List<Opiniao> listAll() {
        return em.createQuery("select o from Opiniao o where o.pergunta1 <> ''", Opiniao.class).getResultList();
    }

    public Opiniao getById(String id) {
        TypedQuery<Opiniao> query = em.createQuery("select o from Opiniao o where o.idOpiniaoUnico=:id", Opiniao.class);
        query.setParameter("id", id);
        return query.getSingleResult();
    }

    public List<Opiniao> getByClienteId(Long cliente_id) {
        TypedQuery<Opiniao> query = em.createQuery("select o from Opiniao o where o.cliente.id=:cliente_id", Opiniao.class);
        query.setParameter("cliente_id", cliente_id);
        return query.getResultList();
    }
    
}
