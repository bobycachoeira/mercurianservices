package br.com.bobycachoeira.dao;

import br.com.bobycachoeira.model.TipoServico;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import java.util.List;

/**
 * Created by boby on 25/09/15.
 */
@Stateless
public class TipoServicoDAO {

    @PersistenceContext
    private EntityManager em;

    public TipoServico save(TipoServico servico){
        em.persist(servico);
        return servico;
    }

    public TipoServico update(TipoServico servico) {
        em.merge(servico);
        return servico;
    }

    public void delete(Long id) {
        em.remove(em.getReference(TipoServico.class, id));
    }

    public List<TipoServico> listAll() {
        return em.createQuery("select o from TipoServico o", TipoServico.class).getResultList();
    }

    public TipoServico getById(Long id) {
        TypedQuery<TipoServico> query = em.createQuery("select o from TipoServico o where o.id=:id", TipoServico.class);
        query.setParameter("id", id);
        return query.getSingleResult();
    }

    public List<TipoServico> getByName(String name) {
        TypedQuery<TipoServico> query = em.createQuery("select o from TipoServico o where o.nome like :name", TipoServico.class);
        query.setParameter("name", "%" + name + "%");
        return query.getResultList();
    }
}
