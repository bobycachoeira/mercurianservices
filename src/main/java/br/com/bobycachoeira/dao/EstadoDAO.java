package br.com.bobycachoeira.dao;

import br.com.bobycachoeira.model.Estado;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import java.util.List;

/**
 * Created by boby on 25/09/15.
 */
@Stateless
public class EstadoDAO {

    @PersistenceContext
    private EntityManager em;

    public Estado save(Estado estado) {
        em.persist(estado);
        return estado;
    }

    public Estado update(Estado estado) {
        em.merge(estado);
        return estado;
    }

    public void delete(Long codigo) {
        em.remove(em.getReference(Estado.class, codigo));
    }

    public List<Estado> listAll() {
        return em.createQuery("select o from Estado o", Estado.class).getResultList();
    }

    public Estado getById(Long id) {
        TypedQuery<Estado> query = em.createQuery("select o from Estado o where o.id=:id", Estado.class);
        query.setParameter("id", id);
        return query.getSingleResult();
    }

    public List<Estado> getByName(String name) {
        TypedQuery<Estado> query = em.createQuery("select o from Estado o where o.nome like :name", Estado.class);
        query.setParameter("name", "%"+name+"%");
        return query.getResultList();
    }
}
