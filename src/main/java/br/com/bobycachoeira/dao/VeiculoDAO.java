package br.com.bobycachoeira.dao;

import br.com.bobycachoeira.model.Veiculo;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import java.util.List;

/**
 * Created by boby on 25/09/15.
 */
@Stateless
public class VeiculoDAO {

    @PersistenceContext
    private EntityManager em;

    public Veiculo save(Veiculo veiculo) {
        em.persist(veiculo);
        return veiculo;
    }

    public Veiculo update(Veiculo veiculo) {
        em.merge(veiculo);
        return veiculo;
    }

    public void delete(Long codigo) {
        em.remove(em.getReference(Veiculo.class, codigo));
    }

    public List<Veiculo> listAll() {
        return em.createQuery("select o from Veiculo o", Veiculo.class).getResultList();
    }

    public List<Veiculo> listaPorCliente(Long cliente_id) {
        //return em.createQuery("select o from Veiculo o where o.cliente_id=:cliente_id", Veiculo.class).getResultList();
        TypedQuery<Veiculo> query = em.createQuery("select o from Veiculo o where o.cliente.id=:cliente_id", Veiculo.class);
        query.setParameter("cliente_id", cliente_id);
        return query.getResultList();
    }

    public Veiculo getById(Long id) {
        TypedQuery<Veiculo> query = em.createQuery("select o from Veiculo o where o.id=:id", Veiculo.class);
        query.setParameter("id", id);
        return query.getSingleResult();
    }

    public List<Veiculo> getByName(String name) {
        TypedQuery<Veiculo> query = em.createQuery("select o from Veiculo o where o.nome like :name", Veiculo.class);
        query.setParameter("name", "%"+name+"%");
        return query.getResultList();
    }
}
