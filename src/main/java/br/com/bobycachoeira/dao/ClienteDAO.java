package br.com.bobycachoeira.dao;

import br.com.bobycachoeira.model.Cliente;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import java.util.List;

/**
 * Created by boby on 18/09/15.
 */
@Stateless
public class ClienteDAO {

    @PersistenceContext
    private EntityManager em;

    public Cliente save(Cliente cliente) {
        em.persist(cliente);
        return cliente;
    }

    public void update(Cliente cliente) {
        em.merge(cliente);
    }

    public void delete(Long codigo) {
        em.remove(em.getReference(Cliente.class, codigo));
    }

    public List<Cliente> listAll() {
        return em.createQuery("select o from Cliente o", Cliente.class).getResultList();
    }

    public Cliente getById(Long id) {
        TypedQuery<Cliente> query = em.createQuery("select o from Cliente o where o.id=:id", Cliente.class);
        query.setParameter("id", id);
        return query.getSingleResult();
    }

    public List<Cliente> getByName(String name) {
        TypedQuery<Cliente> query = em.createQuery("select o from Cliente o where o.nome like :name", Cliente.class);
        query.setParameter("name", "%"+name+"%");
        return query.getResultList();
    }
}
