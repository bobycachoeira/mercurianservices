package br.com.bobycachoeira.dao;

import br.com.bobycachoeira.model.Cidade;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import java.util.List;

/**
 * Created by boby on 22/09/15.
 */
@Stateless
public class CidadeDAO {

    @PersistenceContext
    private EntityManager em;

    public Cidade save(Cidade cidade){
        em.persist(cidade);
        return cidade;
    }

    public Cidade update(Cidade cidade) {
        em.merge(cidade);
        return cidade;
    }

    public void delete(Long id) {
        em.remove(em.getReference(Cidade.class, id));
    }

    public List<Cidade> listAll() {
        return em.createQuery("select o from Cidade o", Cidade.class).getResultList();
    }

    public Cidade getById(Long id) {
        TypedQuery<Cidade> query = em.createQuery("select o from Cidade o where o.id=:id", Cidade.class);
        query.setParameter("id", id);
        return query.getSingleResult();
    }

    public List<Cidade> getByName(String name) {
        TypedQuery<Cidade> query = em.createQuery("select o from Cidade o where o.nome like :name", Cidade.class);
        query.setParameter("name", "%" + name + "%");
        return query.getResultList();
    }
}
