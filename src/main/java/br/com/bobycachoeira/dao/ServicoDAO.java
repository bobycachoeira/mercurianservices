package br.com.bobycachoeira.dao;

import br.com.bobycachoeira.model.Servico;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import java.util.List;

/**
 * Created by boby on 25/09/15.
 */
@Stateless
public class ServicoDAO {


    @PersistenceContext
    private EntityManager em;

    public Servico save(Servico servico){
        em.persist(servico);
        return servico;
    }

    public Servico update(Servico servico) {
        em.merge(servico);
        return servico;
    }

    public void delete(Long id) {
        em.remove(em.getReference(Servico.class, id));
    }

    public List<Servico> listAll() {
        return em.createQuery("select o from Servico o", Servico.class).getResultList();
    }

    public Servico getById(Long id) {
        TypedQuery<Servico> query = em.createQuery("select o from Servico o where o.id=:id", Servico.class);
        query.setParameter("id", id);
        return query.getSingleResult();
    }

    public List<Servico> getByName(String name) {
        TypedQuery<Servico> query = em.createQuery("select o from Servico o where o.nome like :name", Servico.class);
        query.setParameter("name", "%" + name + "%");
        return query.getResultList();
    }
}