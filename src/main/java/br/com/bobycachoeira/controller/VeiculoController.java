package br.com.bobycachoeira.controller;

import br.com.bobycachoeira.dao.VeiculoDAO;
import br.com.bobycachoeira.model.Veiculo;

import javax.ejb.Stateless;
import javax.inject.Inject;
import java.util.List;

/**
 * Created by boby on 25/09/15.
 */
@Stateless
public class VeiculoController {

    @Inject
    private VeiculoDAO dao;

    //Metodo para buscar a cidade pelo seu ID.
    public Veiculo getById(Long id) {
        return dao.getById(id);
    }

    //Metodo para buscar a cidade pelo seu ID.
    public List<Veiculo> getByName(String name) {
        return dao.getByName(name);
    }

    //Metodo para salvar a cidade
    public Veiculo save(Veiculo veiculo) {
        dao.save(veiculo);
        return veiculo;
    }

    //Metodo para Alterar uma cidade ja existente
    public Veiculo update(Veiculo veiculo) {
        dao.update(veiculo);
        return veiculo;
    }

    public void delete(Long id) {
        dao.delete(id);
    }

    public List<Veiculo> listAll() {
        return dao.listAll();
    }

    public List<Veiculo> listaPorCliente(Long id) {
        return dao.listaPorCliente(id);
    }
}
