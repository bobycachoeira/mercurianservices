package br.com.bobycachoeira.controller;

import br.com.bobycachoeira.dao.GestorDAO;
import br.com.bobycachoeira.model.Gestor;

import javax.ejb.Stateless;
import javax.inject.Inject;
import java.util.List;

/**
 * Created by boby on 12/10/15.
 */

@Stateless
public class GestorController {

    @Inject
    private GestorDAO dao;

    //Metodo para buscar o gestor pelo seu ID.
    public Gestor getById(Long id) {
        return dao.getById(id);
    }

    //Metodo para buscar o gestor pelo seu ID.
    public List<Gestor> getByName(String name) {
        return dao.getByName(name);
    }

    //Metodo para salvar o gestor
    public Gestor save(Gestor gestor) {
        dao.save(gestor);
        return gestor;
    }

    //Metodo para Alterar um gestor ja existente
    public Gestor update(Gestor gestor) {
        dao.update(gestor);
        return gestor;
    }

    public void delete(Long id) {
        dao.delete(id);
    }

    public List<Gestor> listAll() {
        return dao.listAll();
    }
}
