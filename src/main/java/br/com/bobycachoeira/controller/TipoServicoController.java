package br.com.bobycachoeira.controller;

import br.com.bobycachoeira.dao.TipoServicoDAO;
import br.com.bobycachoeira.model.TipoServico;

import javax.ejb.Stateless;
import javax.inject.Inject;
import java.util.List;

/**
 * Created by boby on 25/09/15.
 */
@Stateless
public class TipoServicoController {

    @Inject
    private TipoServicoDAO dao;

    //Metodo para buscar o servico pelo seu ID.
    public TipoServico getById(Long id) {
        return dao.getById(id);
    }

    //Metodo para buscar o servico pelo seu ID.
    public List<TipoServico> getByName(String name) {
        return dao.getByName(name);
    }

    //Metodo para salvar o servico
    public TipoServico save(TipoServico servico) {
        dao.save(servico);
        return servico;
    }

    //Metodo para Alterar umo servico ja existente
    public TipoServico update(TipoServico servico) {
        dao.update(servico);
        return servico;
    }

    public void delete(Long id) {
        dao.delete(id);
    }

    public List<TipoServico> listAll() {
        return dao.listAll();
    }
}
