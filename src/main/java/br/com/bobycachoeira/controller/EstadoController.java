package br.com.bobycachoeira.controller;

import br.com.bobycachoeira.dao.EstadoDAO;
import br.com.bobycachoeira.model.Estado;

import javax.ejb.Stateless;
import javax.inject.Inject;
import java.util.List;

/**
 * Created by boby on 25/09/15.
 */
@Stateless
public class EstadoController {

    @Inject
    private EstadoDAO dao;

    //Metodo para buscar a cidade pelo seu ID.
    public Estado getById(Long id) {
        return dao.getById(id);
    }

    //Metodo para buscar a cidade pelo seu ID.
    public List<Estado> getByName(String name) {
        return dao.getByName(name);
    }

    //Metodo para salvar a cidade
    public Estado save(Estado estado) {
        dao.save(estado);
        return estado;
    }

    //Metodo para Alterar uma cidade ja existente
    public Estado update(Estado estado) {
        dao.update(estado);
        return estado;
    }

    public void delete(Long id) {
        dao.delete(id);
    }

    public List<Estado> listAll() {
        return dao.listAll();
    }
}
