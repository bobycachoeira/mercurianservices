package br.com.bobycachoeira.controller;

import br.com.bobycachoeira.model.*;
import org.apache.commons.mail.EmailException;

import javax.ejb.Stateless;
import javax.inject.Inject;
import java.util.Date;

/**
 * Created by boby on 15/10/15.
 */

@Stateless
public class EmailClienteController {

    @Inject
    private ClienteController clienteController;

    @Inject
    private TipoServicoController tipoServicoController;


    //Metodo para montar o objeto email pelo ID da opiniao.
    public EmailCliente montaEmail(Opiniao opiniao, Long idTipoServico) {
        //Cria objeto cliente
        Cliente cliente = new Cliente();
        //Cria objeto do tipo do servico efetuado
        TipoServico tipoServico = new TipoServico();

        cliente = clienteController.getById(opiniao.getCliente().getId());
        tipoServico = tipoServicoController.getById(idTipoServico);


        EmailCliente emailCliente = new EmailCliente();
        emailCliente.setAssunto("Opinião a respeito do servico da empresa Somar PNEUS");
        emailCliente.setEmailDe("posvendasomar@gmail.com");
        emailCliente.setEmailPara(cliente.getEmail());
        emailCliente.setCliente(cliente);
        emailCliente.setTipoServico(tipoServico);
        //emailCliente.setMensagem("http://localhost:8080/mercurianservices-0.0.1-SNAPSHOT/opiniaocliente/opiniao.html#/" + opiniao.getIdOpiniaoUnico());
        emailCliente.setMensagem("Ola " + cliente.getNome() + " muito obrigado por confiar na Somar Pneus"
                + "\n\nEsperamos que possa nos ajudar, respondendo ao questionario e deixar sua opinião" +
                " sobre nosso atendimento e serviços, acesse e responda:" +
                "\nhttp://localhost:8080/mercurianservices-0.0.1-SNAPSHOT/opiniaocliente/opiniao.html#/" + opiniao.getIdOpiniaoUnico()+
                "\n \nQueremos agradecer por ter confiado seu veiculo para que fosse efetuado o servico" +
                " de: "+ tipoServico.getNome());
        //Retorna o email para ser enviado
        return emailCliente;
    }

    public EmailCliente montaEmailGestor(Gestor gestor){

        EmailCliente emailCliente = new EmailCliente();
        emailCliente.setAssunto("Opiniao de cliente inserida no sistema");
        emailCliente.setEmailDe("posvendasomar@gmail.com");
        emailCliente.setEmailPara(gestor.getEmail());
        emailCliente.setMensagem("Ola Gestor foi inserida uma nova opiniao no sistema, e a mesma ainda nao foi baixada.\n" +
                " Acesse o sistema e verifique as opiniões dos clientes que ainda não resolvidas.");
        //Retorna o email para ser enviado
        return emailCliente;
    }

    //Metodo para enviar Email
    public void enviaEmail(EmailCliente emailCliente) throws EmailException {
        EnviaEmail e = new EnviaEmail();
        e.enviaEmail(emailCliente);
    }


}
