package br.com.bobycachoeira.controller;

import br.com.bobycachoeira.model.Opiniao;
import br.com.bobycachoeira.model.Pergunta1DTO;

import javax.ejb.Stateless;
import javax.inject.Inject;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by boby on 18/10/15.
 */

@Stateless
public class Pergunta1Controller {

    @Inject
    private OpiniaoController opiniaoController;

    private Opiniao opiniao = new Opiniao();

    //variaveis dos totais de cada pergunta
    private Double otimo = 0.0;
    private Double bom = 0.0;
    private Double regular = 0.0;
    private Double ruim = 0.0;
    private Double pessimo = 0.0;

    //Variavel do total de opinioes existentes
    private Double totalOpinioes = 0.0;

    private String valorOpiniao;

    public Pergunta1DTO retornaDados() {
        //Cria um objeto Pergunta1DTO para ser alimentado com os valores
        Pergunta1DTO pergunta1 = new Pergunta1DTO();
        List<Opiniao> listaOpiniao = new ArrayList<>();

        //Busca todas as opinioes do banco
        try {
            listaOpiniao = opiniaoController.listAll();
        } catch (Exception e) {
            System.out.print("Ocorreu problema ao buscar opiniao: " + e.getMessage());
        }

        for (int i = 0; i < listaOpiniao.size(); i++){
            //Alimenta o objeto opiniao
            opiniao = listaOpiniao.get(i);
            valorOpiniao = opiniao.getPergunta1();

            //Alimenta o numero de cada opiniao
            if (valorOpiniao.equals("Otimo")) {
                otimo++;
            } else if (valorOpiniao.equals("Bom")) {
                bom++;
            } else if (valorOpiniao.equals("Regular")) {
                regular++;
            } else if (valorOpiniao.equals("Ruim")) {
                ruim++;
            } else if (valorOpiniao.equals("Pessimo")) {
                pessimo++;
            }
            //Alimenta o numero total de opinioes existentes
            totalOpinioes++;
        }

        //Alimenta o valor em percentual das opinioes

        //Alimenta o objeto pergunta1
        pergunta1.setNumeroOpinioes(totalOpinioes);
        pergunta1.setOtimo(otimo);
        pergunta1.setBom(bom);
        pergunta1.setRegular(regular);
        pergunta1.setRuim(ruim);
        pergunta1.setPessimo(pessimo);

        //retorna o objeto pergunta11 alimentado
        return pergunta1;
    }
}
