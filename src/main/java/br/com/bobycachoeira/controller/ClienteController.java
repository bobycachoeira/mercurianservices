package br.com.bobycachoeira.controller;

import br.com.bobycachoeira.dao.ClienteDAO;
import br.com.bobycachoeira.model.Cliente;

import javax.ejb.Stateless;
import javax.inject.Inject;
import java.util.List;

/**
 * Created by boby on 17/09/15.
 */

@Stateless
public class ClienteController {

    @Inject
    private ClienteDAO dao;

    //Metodo para buscar o cliente pelo seu ID.
    public Cliente getById(Long id) {
        return dao.getById(id);
    }

    //Metodo para buscar o cliente pelo seu ID.
    public List<Cliente> getByName(String name) {
        return dao.getByName(name);
    }

    //Metodo para salvar o Cliente
    public Cliente save(Cliente cliente) {
        dao.save(cliente);
        return cliente;
    }

    //Metodo para Alterar um cliente ja existente
    public Cliente update(Cliente cliente) {
        dao.update(cliente);
        return cliente;
    }

    public void delete(Long id) {
        dao.delete(id);
    }

    public List<Cliente> listAll() {
        return dao.listAll();
    }
}
