package br.com.bobycachoeira.controller;

import br.com.bobycachoeira.dao.ServicoDAO;
import br.com.bobycachoeira.model.*;

import javax.ejb.Stateless;
import javax.inject.Inject;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by boby on 25/09/15.
 */
@Stateless
public class ServicoController {

    @Inject
    private ServicoDAO dao;

    @Inject
    private OpiniaoController opiniaoController;

    @Inject
    private EmailClienteController emailClienteController;


    //Metodo para buscar o servico pelo seu ID.
    public Servico getById(Long id) {
        return dao.getById(id);
    }

    //Metodo para buscar o servico pelo seu ID.
    public List<Servico> getByName(String name) {
        return dao.getByName(name);
    }

    //Metodo para salvar o servico
    public Servico save(Servico servico) throws Exception {
        //Cria objeto opiniao para que seja gravado no banco
        Opiniao opiniao = new Opiniao();

        //Alimente o objeto opiniao
        opiniao.setBaixada(false);
        opiniao.setCliente(servico.getCliente());
        opiniao.setDataCriacao(new Date());
        opiniao.setOpiniao("");
        opiniao.setPergunta1("");
        opiniao.setPergunta2("");
        opiniao.setPergunta3("");
        opiniao.setPergunta4("");
        opiniao.setPergunta5(null);

        //Salva o servico no banco
        try {
            dao.save(servico);
        } catch (Exception e) {
            System.out.print("Ocorreu problema na gravacao do servico em: " + e.getMessage());
            return null;
        }


        //Manda salvar a opiniao no banco.
        try {
            opiniaoController.save(opiniao);
        } catch (Exception e) {
            System.out.print("Ocorreu problema na gravacao da opiniao em: " + e.getMessage());
        }


        //Monta email e chama metodo para enviar Enviar o email

        try {
                EmailCliente emailCliente = new EmailCliente();
                emailCliente = emailClienteController.montaEmail(opiniao, servico.getServico().getId());
            if(emailCliente.getEmailPara() != null){
                emailClienteController.enviaEmail(emailCliente);
            }


        } catch (Exception e) {
            System.out.print("Ocorreu problema ao enviar email em: " + e.getMessage());
        }

        //Retorna o servico salvo.
        return servico;
    }

    //Metodo para Alterar umo servico ja existente
    public Servico update(Servico servico) {
        dao.update(servico);

        //Retorna o servico alterado.
        return servico;
    }

    public void delete(Long id) {
        dao.delete(id);
    }

    public List<Servico> listAll() {
        return dao.listAll();
    }
}