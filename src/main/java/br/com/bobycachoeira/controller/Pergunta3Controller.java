package br.com.bobycachoeira.controller;

import br.com.bobycachoeira.model.Opiniao;
import br.com.bobycachoeira.model.Pergunta3DTO;

import javax.ejb.Stateless;
import javax.inject.Inject;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by boby on 18/10/15.
 */

@Stateless
public class Pergunta3Controller {

    @Inject
    private OpiniaoController opiniaoController;

    
    private Opiniao opiniao = new Opiniao();

    //variaveis dos totais de cada pergunta
    private Double otimo = 0.0;
    private Double bom = 0.0;
    private Double regular = 0.0;
    private Double ruim = 0.0;
    private Double pessimo = 0.0;

    //Variavel do total de opinioes existentes
    private Double totalOpinioes = 0.0;

    private String valorOpiniao;


    public Pergunta3DTO retornaDados() {
        //Cria um objeto pergunta3DTO para ser alimentado com os valores
        Pergunta3DTO pergunta3 = new Pergunta3DTO();
        List<Opiniao> listaOpiniao = new ArrayList<>();

        //Busca todas as opinioes do banco
        try {
            listaOpiniao = opiniaoController.listAll();
        } catch (Exception e) {
            System.out.print("Ocorreu problema ao buscar opiniao: " + e.getMessage());
        }

        for (int i = 0; i < listaOpiniao.size(); i++){
            //Alimenta o objeto opiniao
            opiniao = listaOpiniao.get(i);
            valorOpiniao = opiniao.getPergunta3();

            //Alimenta o numero de cada opiniao
            if (valorOpiniao.equals("Otimo")) {
                otimo++;
            } else if (valorOpiniao.equals("Bom")) {
                bom++;
            } else if (valorOpiniao.equals("Regular")) {
                regular++;
            } else if (valorOpiniao.equals("Ruim")) {
                ruim++;
            } else if (valorOpiniao.equals("Pessimo")) {
                pessimo++;
            }
            //Alimenta o numero total de opinioes existentes
            totalOpinioes++;
        }

        //Alimenta o objeto pergunta3
        pergunta3.setNumeroOpinioes(totalOpinioes);
        pergunta3.setOtimo(otimo);
        pergunta3.setBom(bom);
        pergunta3.setRegular(regular);
        pergunta3.setRuim(ruim);
        pergunta3.setPessimo(pessimo);

        //retorna o objeto pergunta3 alimentado
        return pergunta3;
    }
}
