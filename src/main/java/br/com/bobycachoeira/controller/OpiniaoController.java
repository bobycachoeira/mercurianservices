package br.com.bobycachoeira.controller;

import br.com.bobycachoeira.dao.OpiniaoDAO;
import br.com.bobycachoeira.model.EmailCliente;
import br.com.bobycachoeira.model.Gestor;
import br.com.bobycachoeira.model.Opiniao;

import javax.ejb.Stateless;
import javax.inject.Inject;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.UUID;

/**
 * Created by boby on 12/10/15.
 */

@Stateless
public class OpiniaoController {

    @Inject
    private OpiniaoDAO dao;

    @Inject
    private EmailClienteController emailClienteController;

    @Inject
    private GestorController gestorController;


    //Metodo para buscar o opniao pelo seu ID.
    public Opiniao getById(String id) {
        return dao.getById(id);
    }

    //Metodo para buscar o opniao pelo seu ID.
    public List<Opiniao> getByClienteId(Long id) {
        return dao.getByClienteId(id);
    }

    //Metodo para salvar o opniao
    public Opiniao save(Opiniao opiniao) {
        //Cria objeto opiniao para que seja gravado no banco
        Opiniao opiniaoInserir = new Opiniao();
        opiniaoInserir = opiniao;

        //Alimente o objeto opiniao
        opiniaoInserir.setBaixada(false);
        opiniaoInserir.setIdOpiniaoUnico(UUID.randomUUID().toString());
        /*opiniaoInserir.setCliente(servico.getCliente());*/
        opiniaoInserir.setDataCriacao(new Date());

        //Salva o servico no banco
        dao.save(opiniaoInserir);
        return opiniaoInserir;
    }

    //Metodo para Alterar um opniao ja existente
    public Opiniao update(Opiniao opiniao) {
        //ALtera o campo data desta opniao para que seja gravada no banco
        opiniao.setDataCriacao(new Date());

        dao.update(opiniao);
        return opiniao;
    }

    //Metodo para Alterar a opiniao vinda da pagina do cliente ja existente
    public Opiniao clienteAlterar(Opiniao opiniao) {

        //ALtera o campo data desta opniao para que seja gravada no banco
        opiniao.setDataCriacao(new Date());

        dao.update(opiniao);

        //Monta email para gestores e chama metodo para enviar Enviar o email
        try {
            List<Gestor> gestores = new ArrayList<>();
            gestores = gestorController.listAll();

            for (int i = 0; i < gestores.size(); i++){
                Gestor gestor = new Gestor();
                EmailCliente emailCliente = new EmailCliente();
                gestor = gestores.get(i);
                emailCliente = emailClienteController.montaEmailGestor(gestor);
                emailClienteController.enviaEmail(emailCliente);
            }


        } catch (Exception e) {
            System.out.print("Ocorreu problema ao enviar email em: " + e.getMessage());
        }


        return opiniao;
    }

    //Metodo que dara baixa na opiniao
    public Opiniao baixa(Opiniao opiniao){
        //Cria objeto opiniao para que seja gravado no banco
        Opiniao opiniaoBaixar = new Opiniao();
        opiniaoBaixar = opiniao;

        //Altera o flag de baixada para TRUE para gravar
        opiniaoBaixar.setBaixada(true);

        dao.update(opiniaoBaixar);
        return opiniao;
    }

    public void delete(Long id) {
        dao.delete(id);
    }

    public List<Opiniao> listAll() {
        return dao.listAll();
    }
}
