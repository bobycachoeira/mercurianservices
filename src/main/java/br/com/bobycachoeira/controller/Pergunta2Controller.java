package br.com.bobycachoeira.controller;

import br.com.bobycachoeira.model.Opiniao;
import br.com.bobycachoeira.model.Pergunta2DTO;

import javax.ejb.Stateless;
import javax.inject.Inject;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by boby on 18/10/15.
 */

@Stateless
public class Pergunta2Controller {

    @Inject
    private OpiniaoController opiniaoController;

    private Opiniao opiniao = new Opiniao();

    //variaveis dos totais de cada pergunta
    private Double sim = 0.0;
    private Double nao = 0.0;
    private Double talves = 0.0;

    //Variavel do total de opinioes existentes
    private Double totalOpinioes = 0.0;

    private String valorOpiniao;

    public Pergunta2DTO retornaDados() {
        //Cria um objeto Pergunta1DTO para ser alimentado com os valores
        Pergunta2DTO pergunta2 = new Pergunta2DTO();
        List<Opiniao> listaOpiniao = new ArrayList<>();

        //Busca todas as opinioes do banco
        try {
            listaOpiniao = opiniaoController.listAll();
        } catch (Exception e) {
            System.out.print("Ocorreu problema ao buscar opiniao: " + e.getMessage());
        }

        for (int i = 0; i < listaOpiniao.size(); i++){
            //Alimenta o objeto opiniao
            opiniao = listaOpiniao.get(i);
            valorOpiniao = opiniao.getPergunta2();

            //Alimenta o numero de cada opiniao
            if (valorOpiniao.equals("Sim")) {
                sim++;
            } else if (valorOpiniao.equals("Nao")) {
                nao++;
            } else if (valorOpiniao.equals("Talvez")) {
                talves++;
            }

            //Alimenta o numero total de opinioes existentes
            totalOpinioes++;
        }

        //Alimenta o objeto pergunta1
        pergunta2.setNumeroOpinioes(totalOpinioes);
        pergunta2.setSim(sim);
        pergunta2.setNao(nao);
        pergunta2.setTalvez(talves);

        //retorna o objeto pergunta2 alimentado
        return pergunta2;
    }

}
