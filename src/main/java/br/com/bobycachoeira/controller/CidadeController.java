package br.com.bobycachoeira.controller;

import br.com.bobycachoeira.dao.CidadeDAO;
import br.com.bobycachoeira.model.Cidade;

import javax.ejb.Stateless;
import javax.inject.Inject;
import java.util.List;

/**
 * Created by boby on 22/09/15.
 */
@Stateless
public class CidadeController {

    @Inject
    private CidadeDAO dao;

    //Metodo para buscar a cidade pelo seu ID.
    public Cidade getById(Long id) {
        return dao.getById(id);
    }

    //Metodo para buscar a cidade pelo seu ID.
    public List<Cidade> getByName(String name) {
        return dao.getByName(name);
    }

    //Metodo para salvar a cidade
    public Cidade save(Cidade cidade) {
        dao.save(cidade);
        return cidade;
    }

    //Metodo para Alterar uma cidade ja existente
    public Cidade update(Cidade cidade) {
        dao.update(cidade);
        return cidade;
    }

    public void delete(Long id) {
        dao.delete(id);
    }

    public List<Cidade> listAll() {
        return dao.listAll();
    }
}
