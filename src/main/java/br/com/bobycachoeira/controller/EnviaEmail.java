package br.com.bobycachoeira.controller;

import br.com.bobycachoeira.model.EmailCliente;

import java.util.Properties;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

/**
 * Created by boby on 23/10/15.
 */
public class EnviaEmail {

    public void enviaEmail( EmailCliente email) {

        final String username = email.getEmailDe();
        final String password = "trocadepneus";

        Properties props = new Properties();
        props.put("mail.smtp.auth", "true");
        props.put("mail.smtp.starttls.enable", "true");
        props.put("mail.smtp.host", "smtp.gmail.com");
        props.put("mail.smtp.port", "587");

        Session session = Session.getInstance(props,
                new javax.mail.Authenticator() {
                    protected PasswordAuthentication getPasswordAuthentication() {
                        return new PasswordAuthentication(username, password);
                    }
                });

        try {

            Message message = new MimeMessage(session);
            message.setFrom(new InternetAddress(email.getEmailDe()));
            message.setRecipients(Message.RecipientType.TO,
                    InternetAddress.parse(email.getEmailPara()));
            message.setSubject(email.getAssunto());
            message.setText(email.getMensagem());

            Transport.send(message);

        } catch (MessagingException e) {
            throw new RuntimeException(e);
        }
    }

}
