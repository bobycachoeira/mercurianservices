package br.com.bobycachoeira.model;

import org.hibernate.validator.constraints.NotEmpty;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 * Created by boby on 19/09/15.
 */
@Entity
public class Cidade {


    @Id
    @SequenceGenerator(name = "CIDADE_ID", sequenceName = "CIDADE_SEQUENCE", allocationSize = 1)
    @GeneratedValue(strategy= GenerationType.AUTO, generator = "CIDADE_ID")
    @Column(name = "id")
    private Long id;

    @NotNull
    @NotEmpty
    @Column(nullable=false, length=100,name = "nome")
    private String nome;

    @OneToOne
    @JoinColumn(name = "estado_id")
    private Estado estado;


    //Metodo construtor vazio


    /*public Cidade() {
    }
*/
    //Metodos Getter and setter
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public Estado getEstado() {
        return estado;
    }

    public void setEstado(Estado estado) {
        this.estado = estado;
    }
}
