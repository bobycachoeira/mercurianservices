package br.com.bobycachoeira.model;

import org.hibernate.validator.constraints.NotEmpty;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 * Created by boby on 25/09/15.
 */
@Entity
public class Estado {

    @Id
    @SequenceGenerator(name = "ESTADO_ID", sequenceName = "ESTADO_SEQUENCE", allocationSize = 1)
    @GeneratedValue(strategy= GenerationType.AUTO, generator = "ESTADO_ID")
    @Column(name = "id")
    private Long id;

    @NotNull
    @NotEmpty
    @Column(nullable=false, length=20,name = "nome")
    private String nome;

    @NotNull
    @NotEmpty
    @Column(nullable = false,name = "uf")
    @Size(min = 2, max = 2)
    private String uf;

    //Metodos getter e setter


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getUf() {
        return uf;
    }

    public void setUf(String uf) {
        this.uf = uf;
    }
}
