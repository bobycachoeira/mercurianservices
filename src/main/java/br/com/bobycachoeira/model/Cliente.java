package br.com.bobycachoeira.model;

import org.hibernate.validator.constraints.NotEmpty;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Entity
@Table(uniqueConstraints={@UniqueConstraint(columnNames = {"nome" , "email"})})
public class Cliente{

	
	@Id
	@SequenceGenerator(name = "CLIENTE_ID", sequenceName = "CLIENTE_SEQUENCE", allocationSize = 1)
	@GeneratedValue(strategy= GenerationType.AUTO, generator = "CLIENTE_ID")
	@Column(name = "id")
	private Long id;

	@NotNull
	@NotEmpty
	@Column(nullable=false, length=100,name = "nome")
	private String nome;


	@Column(name = "email")
	@Size( max = 50)
	private String email;

	@Column(name = "telefone")
	private String telefone;

	@Column(name = "telefone2")
	private String telefone2;

	@OneToOne
	@JoinColumn(nullable=false,name = "cidade_id")
	private Cidade cidade;
	
	
	//Metodo construtor Vazio
	public Cliente() {
		super();
	}

	//Metodo construtor valorado
	public Cliente(Long id) {
		super();
		this.id = id;
	}	
	

	//Metodos Getter and Setter
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getTelefone() {
		return telefone;
	}

	public void setTelefone(String telefone) {
		this.telefone = telefone;
	}

	public String getTelefone2() {
		return telefone2;
	}

	public void setTelefone2(String telefone2) {
		this.telefone2 = telefone2;
	}

	public Cidade getCidade() {
		return cidade;
	}

	public void setCidade(Cidade cidade) {
		this.cidade = cidade;
	}
}