package br.com.bobycachoeira.model;

/**
 * Created by boby on 18/10/15.
 */
public class Pergunta1DTO {
    private Double otimo;
    private Double bom;
    private Double regular;
    private Double ruim;
    private Double pessimo;

    private Double numeroOpinioes;

    public Double getOtimo() {
        return otimo;
    }

    public void setOtimo(Double otimo) {
        this.otimo = otimo;
    }

    public Double getBom() {
        return bom;
    }

    public void setBom(Double bom) {
        this.bom = bom;
    }

    public Double getRegular() {
        return regular;
    }

    public void setRegular(Double regular) {
        this.regular = regular;
    }

    public Double getRuim() {
        return ruim;
    }

    public void setRuim(Double ruim) {
        this.ruim = ruim;
    }

    public Double getPessimo() {
        return pessimo;
    }

    public void setPessimo(Double pessimo) {
        this.pessimo = pessimo;
    }

    public Double getNumeroOpinioes() {
        return numeroOpinioes;
    }

    public void setNumeroOpinioes(Double numeroOpinioes) {
        this.numeroOpinioes = numeroOpinioes;
    }
}
