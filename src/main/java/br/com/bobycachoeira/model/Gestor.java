package br.com.bobycachoeira.model;

import org.hibernate.validator.constraints.NotEmpty;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 * Created by boby on 12/10/15.
 */
@Entity
public class Gestor {

    @Id
    @SequenceGenerator(name = "GESTOR_ID", sequenceName = "GESTOR_SEQUENCE", allocationSize = 1)
    @GeneratedValue(strategy= GenerationType.AUTO, generator = "GESTOR_ID")
    @Column(name = "id")
    private Long id;

    @NotNull
    @NotEmpty
    @Column(nullable=false, length=100,name = "nome")
    private String nome;


    @NotNull
    @NotEmpty
    @Column(nullable=false, name = "email")
    @Size( max = 50)
    private String email;

    //Metodos Getter e Setter
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}
