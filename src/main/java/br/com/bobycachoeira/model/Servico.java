package br.com.bobycachoeira.model;

import org.hibernate.validator.constraints.NotEmpty;

import javax.persistence.*;

/**
 * Created by boby on 25/09/15.
 */
@Entity
public class Servico {


    @Id
    @SequenceGenerator(name = "SERVICO_ID", sequenceName = "SERVICO_SEQUENCE", allocationSize = 1)
    @GeneratedValue(strategy= GenerationType.AUTO, generator = "SERVICO_ID")
    @Column(name = "id")
    private Long id;


    @Column(length=100,name = "observacao")
    private String observacao;

    @OneToOne
    @JoinColumn(name = "cliente_id")
    private Cliente cliente;

    @OneToOne
    @JoinColumn(name = "veiculo_id")
    private Veiculo veiculo;

    @OneToOne
    @JoinColumn(name = "servico_id")
    private TipoServico tipoServico;

    //Metodos Getter e Setter


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getObservacao() {
        return observacao;
    }

    public void setObservacao(String observacao) {
        this.observacao = observacao;
    }

    public Cliente getCliente() {
        return cliente;
    }

    public void setCliente(Cliente cliente) {
        this.cliente = cliente;
    }

    public Veiculo getVeiculo() {
        return veiculo;
    }

    public void setVeiculo(Veiculo veiculo) {
        this.veiculo = veiculo;
    }

    public TipoServico getServico() {
        return tipoServico;
    }

    public void setServico(TipoServico servico) {
        this.tipoServico = servico;
    }
}