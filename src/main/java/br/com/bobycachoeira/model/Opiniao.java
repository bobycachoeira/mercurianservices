package br.com.bobycachoeira.model;

import org.hibernate.validator.constraints.NotEmpty;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.Date;

/**
 * Created by boby on 12/10/15.
 */

@Entity
public class Opiniao {

    @Id
    @SequenceGenerator(name = "OPINIAO_ID", sequenceName = "OPINIAO_SEQUENCE", allocationSize = 1)
    @GeneratedValue(strategy= GenerationType.AUTO, generator = "OPINIAO_ID")
    @Column(name = "id")
    private Long id;

    @NotEmpty
    @NotNull
    @Column(name = "id_opiniao_unico")
    private String idOpiniaoUnico;

    @Column(length=100,name = "pergunta1")
    private String pergunta1;


    @Column(length=100,name = "pergunta2")
    private String pergunta2;


    @Column(length=100,name = "pergunta3")
    private String pergunta3;

    @Column(length=100,name = "pergunta4")
    private String pergunta4;

    @Column(name = "pergunta5")
    private Long pergunta5;


    @Column(length=500,name = "opiniao")
    private String opiniao;

    @Column(name = "baixada")
    private Boolean baixada;

    @Column(name = "dataCriacao")
    @Temporal(TemporalType.DATE)
    private Date dataCriacao;

    @OneToOne
    @JoinColumn(nullable=false,name = "cliente_id")
    private Cliente cliente;


    //Metodos Getter e Setter


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getIdOpiniaoUnico() {
        return idOpiniaoUnico;
    }

    public void setIdOpiniaoUnico(String idOpiniaoUnico) {
        this.idOpiniaoUnico = idOpiniaoUnico;
    }

    public String getPergunta1() {
        return pergunta1;
    }

    public void setPergunta1(String pergunta1) {
        this.pergunta1 = pergunta1;
    }

    public String getPergunta2() {
        return pergunta2;
    }

    public void setPergunta2(String pergunta2) {
        this.pergunta2 = pergunta2;
    }

    public String getPergunta3() {
        return pergunta3;
    }

    public void setPergunta3(String pergunta3) {
        this.pergunta3 = pergunta3;
    }

    public String getPergunta4() {
        return pergunta4;
    }

    public void setPergunta4(String pergunta4) {
        this.pergunta4 = pergunta4;
    }

    public Long getPergunta5() {
        return pergunta5;
    }

    public void setPergunta5(Long pergunta5) {
        this.pergunta5 = pergunta5;
    }

    public String getOpiniao() {
        return opiniao;
    }

    public void setOpiniao(String opiniao) {
        this.opiniao = opiniao;
    }

    public Boolean getBaixada() {
        return baixada;
    }

    public void setBaixada(Boolean baixada) {
        this.baixada = baixada;
    }

    public Date getDataCriacao() {
        return dataCriacao;
    }

    public void setDataCriacao(Date dataCriacao) {
        this.dataCriacao = dataCriacao;
    }

    public Cliente getCliente() {
        return cliente;
    }

    public void setCliente(Cliente cliente) {
        this.cliente = cliente;
    }
}
