package br.com.bobycachoeira.model;

/**
 * Created by boby on 18/10/15.
 */
public class Pergunta2DTO {
    private Double sim;
    private Double nao;
    private Double talvez;

    private Double numeroOpinioes;

    public Double getSim() {
        return sim;
    }

    public void setSim(Double sim) {
        this.sim = sim;
    }

    public Double getNao() {
        return nao;
    }

    public void setNao(Double nao) {
        this.nao = nao;
    }

    public Double getTalvez() {
        return talvez;
    }

    public void setTalvez(Double talvez) {
        this.talvez = talvez;
    }

    public Double getNumeroOpinioes() {
        return numeroOpinioes;
    }

    public void setNumeroOpinioes(Double numeroOpinioes) {
        this.numeroOpinioes = numeroOpinioes;
    }
}
