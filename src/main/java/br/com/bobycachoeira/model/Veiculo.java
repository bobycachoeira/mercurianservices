package br.com.bobycachoeira.model;

import org.hibernate.validator.constraints.NotEmpty;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 * Created by boby on 25/09/15.
 */
@Entity
public class Veiculo {

    @Id
    @SequenceGenerator(name = "VEICULO_ID", sequenceName = "VEICULO_SEQUENCE", allocationSize = 1)
    @GeneratedValue(strategy= GenerationType.AUTO, generator = "VEICULO_ID")
    @Column(name = "id")
    private Long id;

    @NotNull
    @NotEmpty
    @Column(nullable=false, length=20,name = "marca")
    private String marca;

    @NotNull
    @NotEmpty
    @Column(nullable=false, length=20,name = "modelo")
    private String modelo;


    @Column(name = "ano")
    private Long ano;


    @Column(length=20,name = "placa")
    private String placa;

    @OneToOne
    @JoinColumn(name = "cliente_id")
    private Cliente cliente;


    //Metodos Getter e Setter
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getMarca() {
        return marca;
    }

    public void setMarca(String marca) {
        this.marca = marca;
    }

    public String getModelo() {
        return modelo;
    }

    public void setModelo(String modelo) {
        this.modelo = modelo;
    }

    public Long getAno() {
        return ano;
    }

    public void setAno(Long ano) {
        this.ano = ano;
    }

    public String getPlaca() {
        return placa;
    }

    public void setPlaca(String placa) {
        this.placa = placa;
    }

    public Cliente getCliente() {
        return cliente;
    }

    public void setCliente(Cliente cliente) {
        this.cliente = cliente;
    }
}
