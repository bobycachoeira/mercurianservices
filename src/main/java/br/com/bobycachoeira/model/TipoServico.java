package br.com.bobycachoeira.model;

import org.hibernate.validator.constraints.NotEmpty;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

/**
 * Created by boby on 25/09/15.
 */
@Entity
public class TipoServico {

    @Id
    @SequenceGenerator(name = "TIPO_SERVICO_ID", sequenceName = "TIPO_SERVICO_SEQUENCE", allocationSize = 1)
    @GeneratedValue(strategy= GenerationType.AUTO, generator = "TIPO_SERVICO_ID")
    @Column(name = "id")
    private Long id;

    @NotNull
    @NotEmpty
    @Column(nullable=false, length=35,name = "nome")
    private String nome;

    @NotNull
    @NotEmpty
    @Column(nullable=false, length=500,name = "descricao")
    private String descricao;

    //Metodos Getter e Setter

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }
}
