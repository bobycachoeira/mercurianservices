package br.com.bobycachoeira.model;


import java.util.Date;

/**
 * Created by boby on 15/10/15.
 */

public class EmailCliente {

    private String emailDe;
    private String emailPara;
    private String assunto;
    private String mensagem;
    private Opiniao opiniao;
    private Cliente cliente;
    private TipoServico tipoServico;

    public String getEmailDe() {
        return emailDe;
    }

    public void setEmailDe(String emailDe) {
        this.emailDe = emailDe;
    }

    public String getEmailPara() {
        return emailPara;
    }

    public void setEmailPara(String emailPara) {
        this.emailPara = emailPara;
    }

    public String getAssunto() {
        return assunto;
    }

    public void setAssunto(String assunto) {
        this.assunto = assunto;
    }

    public String getMensagem() {
        return mensagem;
    }

    public void setMensagem(String mensagem) {
        this.mensagem = mensagem;
    }

    public Opiniao getOpiniao() {
        return opiniao;
    }

    public void setOpiniao(Opiniao opiniao) {
        this.opiniao = opiniao;
    }

    public Cliente getCliente() {
        return cliente;
    }

    public void setCliente(Cliente cliente) {
        this.cliente = cliente;
    }

    public TipoServico getTipoServico() {
        return tipoServico;
    }

    public void setTipoServico(TipoServico tipoServico) {
        this.tipoServico = tipoServico;
    }
}

