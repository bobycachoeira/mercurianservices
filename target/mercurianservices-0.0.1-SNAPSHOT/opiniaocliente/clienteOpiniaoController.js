'use strict';

var servidor = "http://localhost:8080/mercurianservices-0.0.1-SNAPSHOT/api";
//var servidor = "http://mercurianservices-bobycachoeira.rhcloud.com/api";
var opiniaoAlterar;

/**
 * @ngdoc overview
 * @name mercurianBobyApp
 * @description
 * # mercurianBobyApp
 *
 * Main module of the application.
 */
angular
    .module('mercurianBobyAppCliente', [
        'ngAnimate',
        'ngAria',
        'ngCookies',
        'ngMessages',
        'ngResource',
        'ngRoute',
        'ngSanitize',
        'ngTouch',
        'ngMaterial'
    ])
    .config(function ($routeProvider, $httpProvider) {
        $routeProvider
            .when('/:id', {
                templateUrl: 'views/clienteOpiniao.html',
                controller: 'ClienteOpiniaoController'
            })
            //Quando for inserido a opiniao
            .when('/terminado/obrigado', {
                templateUrl: 'views/clienteOpiniaoObrigado.html',
                controller: 'ClienteOpiniaoController'
            })
    })

.controller('ClienteOpiniaoController', function GetAllOpiniao($scope, $routeParams, $http, $mdDialog, $mdToast, $location) {
        var last = {
            bottom: false,
            top: true,
            left: false,
            right: true
        };

        var id = $routeParams.id;

        $http.defaults.useXDomain = true;

        $http.get(servidor + '/cliente').
            success(function (data) {
                $scope.clientes = data;
            });


        $http.get(servidor + '/opiniao/pesquisar/' + id).success(function (data) {
            $scope.opiniao = data;
        }).error(function () {
            /*$location.path('/clienteOpiniao/responder/'+id);*/
        });


        //Essa será executada no click do botão edit ("putUser")
        $scope.opiniao = opiniaoAlterar;


        $scope.alteraOpiniao = function (opiniao) {
            $scope.mostraProgressBar = true;
            $scope.messageFinalSalvando();
            $http.put(servidor + '/opiniao/clienteAlterar', opiniao).success(function () {
                $scope.mostraProgressBar = false;
                $scope.messageFinalAltera(opiniao, true);
                $location.path('/terminado/obrigado');
            }).error(function () {
                $scope.mostraProgressBar = false;
                $scope.messageFinalAltera(opiniao, false);
                $location.path('/'+opiniao.id);
            });
        };



        var reset = function () {
            $scope.opiniao = {
                "id": 0,
                "pergunta1": "",
                "pergunta2": "",
                "pergunta3": "",
                "pergunta4": "",
                "pergunta5": null,
                "opniao": "",
                "baixada": false,
                "dataCriacao": "",
                "cliente": {
                    "id": 0,
                    "nome": "",
                    "email": "",
                    "telefone": "",
                    "telefone2": null,
                    "cidade": {
                        "id": 0,
                        "nome": "",
                        "estado": {
                            "id": 0,
                            "nome": "",
                            "uf": ""
                        }
                    }
                }
            };
            opiniaoAlterar = {
                "id": 0,
                "pergunta1": "",
                "pergunta2": "",
                "pergunta3": "",
                "pergunta4": "",
                "pergunta5": null,
                "opniao": "",
                "baixada": false,
                "dataCriacao": "",
                "cliente": {
                    "id": 0,
                    "nome": "",
                    "email": "",
                    "telefone": "",
                    "telefone2": null,
                    "cidade": {
                        "id": 0,
                        "nome": "",
                        "estado": {
                            "id": 0,
                            "nome": "",
                            "uf": ""
                        }
                    }
                }
            };
        };


        /*Toast da Opiniao Alterado*/
        $scope.messageFinalAltera = function (opiniao, t) {
            if (t) {
                $mdToast.show(
                    $mdToast.simple()
                        .content('Opiniao gravada com sucesso, Muito obrigado')
                        .position($scope.getToastPosition())
                        .hideDelay(3000)
                );
            } else {
                $mdToast.show(
                    $mdToast.simple()
                        .content('Problema ao gravar Opiniao tente novamente se desejar!')
                        .position($scope.getToastPosition())
                        .hideDelay(4000)
                );
            }

        };


        /*Toast de mensagem salvando servico Servico Alterado*/
        $scope.messageFinalSalvando = function () {
            $mdToast.show(
                $mdToast.simple()
                    .content('Aguarde que estamos salvando sua opinião, Muito obrigado')
                    .position($scope.getToastPosition())
                    .hideDelay(20000)
            );
        };

        /*Pega a posicao do toast*/
        $scope.toastPosition = angular.extend({}, last);
        $scope.getToastPosition = function () {
            sanitizePosition();
            return Object.keys($scope.toastPosition)
                .filter(function (pos) {
                    return $scope.toastPosition[pos];
                })
                .join(' ');
        };
        /*Sanitazi position*/
        function sanitizePosition() {
            var current = $scope.toastPosition;
            if (current.bottom && last.top) current.top = false;
            if (current.top && last.bottom) current.bottom = false;
            if (current.right && last.left) current.left = false;
            if (current.left && last.right) current.right = false;
            last = angular.extend({}, current);
        }

    })

    /*Controller da sidenav lateral.*/
    .controller('MainCtrl', function ($scope, $timeout, $mdSidenav, $mdUtil, $log) {
        $scope.toggleLeft = buildToggler('left');
        /**
         * Build handler to open/close a SideNav; when animation finishes
         * report completion in console
         */
        function buildToggler(navID) {
            var debounceFn =  $mdUtil.debounce(function(){
                $mdSidenav(navID)
                    .toggle()
                    .then(function () {
                        $log.debug("toggle " + navID + " is done");
                    });
            },200);
            return debounceFn;
        }
    })
    .controller('LeftCtrl', function ($scope, $timeout, $mdSidenav, $log) {
        $scope.closeSideNav = function () {
            $mdSidenav('left').close()
                .then(function () {
                    $log.debug("close LEFT is done");
                });
        };
    });

